package com.mypro.MyHome;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ButtonPage extends Application {
    private static final Text NULL = null;
    @Override
    public void start(Stage prStage) throws Exception {
     
        Text txt=new Text("username");
        txt.setFont(new Font(20));
        TextField tx= new TextField();
        Text pas=new Text("Password");
        pas.setFont(new Font(20));
        PasswordField ps= new PasswordField();
        Button show= new Button("show");

        Label l=new Label();
        l.setText("LOGIN");
        l.setFont(new Font(40));
        l.setTextFill(Color.RED);

        Label lb=new Label();
        lb.setText("username :");
        lb.setFont(new Font(15));
        lb.setTextFill(Color.BLACK);
        
        Label lb1=new Label();
        lb1.setText("Password :");
        lb1.setFont(new Font(15));
        lb1.setTextFill(Color.BLACK);
        

        show.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println(tx.getText());
                System.out.println(ps.getText());
                System.out.println(l.getText());

                lb.setText(tx.getText());
                lb1.setText(ps.getText());
            
            }
        });

        VBox vb= new VBox(20,l,txt,tx,pas,ps,show,lb,lb1);
        vb.setLayoutX(300);
        vb.setLayoutY(300);
        Group gr=new Group(vb);
        Scene sc=new Scene(gr,1000,1000);
        sc.setFill(Color.PINK);
        prStage.setScene(sc);
        prStage.show();


    
        }

}
