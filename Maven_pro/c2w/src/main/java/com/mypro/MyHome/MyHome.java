package com.mypro.MyHome;

import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyHome extends Application {
     Text title=null;
    Text java=null;
    Text python=null;
    @Override
    public void start(Stage prStage){
        prStage.setTitle("MyStage");
        prStage.setHeight(800);
        prStage.setWidth(1000);
        prStage.setResizable(true);
       // prStage.getIcons().add(new Image("C:/Users/Monika/OneDrive/Desktop/Maven_pro/c2w/src/main/resources/Assets/logo.jpeg"));
        
        Text txt=new Text(20,80,"GOOD EVINING");
        txt.setFill(Color.GREEN);
        txt.setFont(new Font(40));
        Text gd=new Text(500,80,"HAVE A NICE DAY !!!");
        gd.setFill(Color.RED);
        gd.setFont(new Font(40));
        
        
        java=new Text(10,200,"java");
        java.setFont(new Font(30));
        java.setFill(Color.BROWN);

        python=new Text(10,250,"python");
        python.setFont(new Font(30));
        python.setFill(Color.BROWN);

        Text cpp=new Text(10,300,"cpp");
        cpp.setFont(new Font(30));
        cpp.setFill(Color.BROWN);
    
        Text lib=new Text(10,200,"lib");
        lib.setFont(new Font(30));
        lib.setFill(Color.BLUE);


        Text bin=new Text(10,250,"bin");
        bin.setFont(new Font(30));
        bin.setFill(Color.BLUE);

        Text src=new Text(10,300,"src");
        src.setFont(new Font(30));
        src.setFill(Color.BLUE);

        VBox vb=new VBox(20,java,python,cpp);
        vb.setLayoutX(10);
        vb.setLayoutY(20);

        VBox vb2=new VBox(20,lib,bin,src);
        vb2.setLayoutX(10);
        vb2.setLayoutY(20);

        HBox hb=new HBox(20,vb,vb2);
        hb.setLayoutX(400);
        hb.setLayoutY(200);

    
        Group gr=new Group(hb,txt,gd);
        Scene sc=new Scene(gr);
        sc.setFill(Color.LIGHTBLUE);
        sc.setCursor(Cursor.OPEN_HAND);
         
        prStage.setScene(sc);

      prStage.show();

    }

}


