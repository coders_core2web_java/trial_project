package com.mypro.MyHome;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MyPane extends Application{

    @Override
    public void start(Stage prStage) throws Exception {
       BorderPane b=new BorderPane(); 

    prStage.setTitle("MyStage");
    prStage.setHeight(800);
    prStage.setWidth(1000);
    prStage.setResizable(true);

    Label top=new Label("os");
    top.setFont(new Font(40));

    Label rt=new Label("oop");
    rt.setFont(new Font(40));
    
    Label rb=new Label("dbms");
    rb.setFont(new Font(40));
    rb.setMaxHeight(1000);

    Label bottom=new Label("dsa");
    bottom.setFont(new Font(40));

    Label center=new Label("center");
    center.setFont(new Font(40));

     VBox rightBox = new VBox();
     rightBox.setAlignment(Pos.BOTTOM_RIGHT);
     rightBox.setAlignment(Pos.TOP_RIGHT);
     rightBox.getChildren().addAll(rt, new VBox(), rb);
    VBox.setVgrow(rightBox.getChildren().get(1), Priority.ALWAYS); 

    b.setTop(top);
    b.setRight(rightBox);
    b.setBottom(bottom);
    b.setCenter(center);


    Scene sc=new Scene(b,0,1000);
        sc.setFill(Color.AQUA);
        prStage.setScene(sc);

      prStage.show();
    
    }

    }
    

