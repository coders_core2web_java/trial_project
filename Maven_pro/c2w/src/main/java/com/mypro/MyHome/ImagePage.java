package com.mypro.MyHome;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ImagePage extends Application {

    @Override
    public void start(Stage prStage) throws Exception {
        prStage.setHeight(800);
        prStage.setWidth(1000);
        prStage.setResizable(true);  

        Image ig=new Image("resources/logo.jpeg");
        ImageView iv=new ImageView(ig);
        iv.setFitHeight(200);
        iv.setFitWidth(500);
        iv.setPreserveRatio(true);

        Label lb=new Label("coders");
        lb.setFont(new Font(30));
        lb.setPrefHeight(200);
        lb.setPrefWidth(500);

        HBox hb=new HBox(12,iv,lb);
        hb.setPrefHeight(200);
        hb.setPrefWidth(400);
        hb.setStyle("-fx-background-color:PINK");

        Group gr= new Group(hb);
        Scene sc=new Scene(gr,Color.LIGHTSEAGREEN);
        prStage.setScene(sc);
        prStage.show();
        
    }

}
    


