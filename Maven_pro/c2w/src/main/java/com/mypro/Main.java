package com.mypro;

import com.mypro.MyHome.ButtonPage;
import com.mypro.MyHome.ImagePage;
import com.mypro.MyHome.MyHome;
import com.mypro.MyHome.MyPane;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        Application.launch(MyHome.class,args);
    }
}